// Đường dẫn tới mockAPI
// Lên API edit lại Todos data không cần tạo mới
// Đừng edit lại kẻo hỏng chấm bài xong có điểm hẵng edit data or tạo mới !!

const BASE_URL = 'https://635f4b173e8f65f283b01181.mockapi.io';

// tạo biến id edit cho phần edit API
var idEdited = null;

// loading function
function loadingOn(){
    document.getElementById('loading').style.display = "flex";
}

function loadingOff(){
    document.getElementById('loading').style.display = "none";
}

// function get info from form
function getThongTin(){
    var nameInput = document.querySelector('#name').value;
    var titleInput = document.querySelector('#title').value;
    var descInput = document.querySelector('#desc').value;
    return {
        name: nameInput,
        title: titleInput,
        desc: descInput,
    }
}

// function push info edit to form
function pushThongTin(api){
    document.querySelector('#name').value = api.data.name;
    document.querySelector('#title').value = api.data.title;
    document.querySelector('#desc').value = api.data.desc;
}

// fetch ngay sau khi loading
fetchAllData();

// function fetch all on loading
function fetchAllData(){
    loadingOn();
    // lấy thông tin từ mockAPI
    axios ({
        url: `${BASE_URL}/api`,
        method: 'GET', 
    })
    .then(
        function (res) {
            loadingOff();
            console.log('data fetched:',res.data);
            renderThongTin(res.data);
        })
    .catch(
        function (err) {
            // error message
    });
}

// function render thông tin lên form

function renderThongTin(api){
    var contentHTML = '';
    api.forEach(function(item){
        var content = 
        `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.title}</td>
            <td>${item.desc}</td>
            <td class="setting">

                <button class="btn btn-danger" onclick="removeAPI(${item.id})" title="Delete">Delete</button>

                <button class="btn btn-warning" onclick="editAPI(${item.id})" title="Edit">Edit</button>

            </td>
        </tr> 
        `;
        contentHTML += content;
    });
    document.querySelector('#tbody-api').innerHTML = contentHTML;
}

// function add API

function addAPI(){ 
    // DOM
    var dataInput = getThongTin();
    // New object coi chừng sai trường check lại data API Schema
    var newAPI = {
        name: dataInput.name,
        title: dataInput.title,
        desc: dataInput.desc,
    };
    loadingOn();
    // khai báo phương thức add = POST
    axios ({
        url: `${BASE_URL}/api`,
        method: 'POST',
        // khai báo object mới thêm vào từ object DOM newAPI
        data: newAPI,  
    })
    .then(
        function (res) {
            // loadingOff();
            console.log('data added:',res.data);
            // reload all
            fetchAllData();
            window.scrollTo(0, document.querySelector(".container").scrollHeight);
        })
    .catch(
        function (err) {
            // error message
    });
}

// function remove API
function removeAPI(idAPI){
    loadingOn();
    axios ({
        url: `${BASE_URL}/api/${idAPI}`,
        method: 'DELETE',  
    })
    .then(
        function (res) {
            // loadingOff();
            console.log('data removed:',res.data);
            // reload all
            fetchAllData();
        })
    .catch(
        function (err) {
            // error message
    });
}

// function edit API
function editAPI(idAPI){
    loadingOn();
    axios ({
        url: `${BASE_URL}/api/${idAPI}`,
        method: 'GET',  
    })
    .then(
        function (res) {
            console.log('data to edit:',res.data);
            // update id edit
            idEdited = res.data.id;
            // push info to form
            pushThongTin(res);
            // scroll lên top đề phòng trường hợp item cần sửa nằm bên dưới
            loadingOff();
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
              });
        })
    .catch(
        function (err) {
            // error message
    });
}

// function update API
function updateAPI(){
    loadingOn();
    // DOM
    let dataEdited = getThongTin();
    // New object
    var editedAPI = {
        name: dataEdited.name,
        title: dataEdited.title,
        desc: dataEdited.desc,
    };
    axios ({
        url: `${BASE_URL}/api/${idEdited}`,
        method: 'PUT', 
        data: editedAPI,
    })
    .then(
        function (res) {
            loadingOff();
            console.log('data updated:',res.data);
            // reload all
            fetchAllData();
        })
    .catch(
        function (err) {
            // error message
    });
}